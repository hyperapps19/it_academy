package hyper.apps;

abstract public class GraphSearch { // Абстрактный класс для поиска в графе
    Graph graph;

    public abstract void doSearch();
}
