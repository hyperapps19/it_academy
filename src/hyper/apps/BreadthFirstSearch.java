package hyper.apps;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class BreadthFirstSearch extends GraphSearch { // Поиск в ширину

    private ArrayDeque<Vertex> deque = new ArrayDeque<>(); // Очередь вершин для посещения
    private ArrayList<Vertex> visited = new ArrayList<>(); // Уже посещённые вершины

    public BreadthFirstSearch(Graph graph) {
        this.graph = graph;
    }


    public void doSearch() {
        Vertex firstVertex = (Vertex) graph.getFirstVertex();
        System.out.println("Помещаем начальную вершину " + firstVertex.getData() + " в конец очереди");
        deque.addLast(firstVertex);
        Vertex currentVertex;
        while (deque.size() > 0) { // Повторяем, пока очередь не станет пустой (нечего посещать)
            System.out.print("Очередь: ");
            deque.forEach(vertex1 -> System.out.print(vertex1.getData() + " "));
            System.out.println();

            currentVertex = deque.removeFirst();
            System.out.println("Посещаем вершину " + currentVertex.getData() + " (из начала очереди)");
            visited.add(currentVertex);

            System.out.print("Посещённые вершины: ");
            visited.forEach(vertex1 -> System.out.print(vertex1.getData() + " "));
            System.out.println();

            for (Object vertex : graph.getNeighbors(currentVertex)) {
                if (!visited.contains(vertex)) {
                    System.out.println("Помещаем смежную с " + currentVertex.getData()
                            + " непосещённую вершину " + ((Vertex) vertex).getData() + " в конец очереди");
                    deque.addLast((Vertex) vertex);
                }
            }
        }
        System.out.println("Очередь пуста! Прекращаем работу...");
    }
}
