package hyper.apps;

class Vertex<T> { // Класс вершины
    private T data; // Имя вершины
    private boolean visited = false; // Посещена ли вершина?

    public Vertex(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public boolean isVisited() {
        return visited;
    }

    public void visit() { // Посетить вершину. Обратное действие уже невозможно
        visited = true;
    }
}

public class Main {


    public static void main(String[] args) {
        // Создаём граф
        Graph<Vertex> graph = new Graph<>();
        // Создаём вершины
        Vertex L = new Vertex<>('L');
        graph.addVertex(L);
        Vertex B = new Vertex<>('B');
        graph.addVertex(B);
        Vertex N = new Vertex<>('N');
        graph.addVertex(N);
        Vertex A = new Vertex<>('A');
        graph.addVertex(A);
        Vertex C = new Vertex<>('C');
        graph.addVertex(C);
        Vertex D = new Vertex<>('D');
        graph.addVertex(D);
        Vertex E = new Vertex<>('E');
        graph.addVertex(E);
        Vertex F = new Vertex<>('F');
        graph.addVertex(F);
        Vertex T = new Vertex<>('T');
        graph.addVertex(T);
        Vertex G = new Vertex<>('G');
        graph.addVertex(G);

        // Связываем вершины
        // Рисунок данного графа в репозитории
        graph.addEdge(A, B);
        graph.addEdge(A, N);
        graph.addEdge(A, L);
        graph.addEdge(B, C);
        graph.addEdge(B, D);
        graph.addEdge(B, E);
        graph.addEdge(N, F);
        graph.addEdge(N, T);
        graph.addEdge(L, G);


        // Запускаем алгоритмы поиска
        System.out.println("======= Алгоритм поиска В ГЛУБИНУ в графе =======");
        GraphSearch dfs = new DepthFirstSearch(graph);
        dfs.doSearch();

        System.out.println("======= Алгоритм поиска В ШИРИНУ в графе =======");
        GraphSearch bfs = new BreadthFirstSearch(graph);
        bfs.doSearch();
    }
}

