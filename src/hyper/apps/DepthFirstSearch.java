package hyper.apps;

public class DepthFirstSearch extends GraphSearch { // Поиск в глубину

    public DepthFirstSearch(Graph graph) {
        this.graph = graph;
    }


    public void iteration(Vertex vertex) {
        if (vertex.isVisited()) return;
        vertex.visit();
        System.out.println("Вершина " + vertex.getData() + " отмечена как посещённая");
        graph.getNeighbors(vertex).forEach(o -> iteration((Vertex) o));
    }


    public void doSearch() {
        for (Object vertex : graph.getAllVertices()) {
            if (!((Vertex) vertex).isVisited()) {
                System.out.println("Начинаем с вершины " + ((Vertex) vertex).getData());
                iteration((Vertex) vertex);
                System.out.println("Возвращаемся к исходной вершине " + ((Vertex) vertex).getData());
            }
        }
    }
}
